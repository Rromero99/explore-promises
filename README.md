<img src="http://i.imgur.com/UzC7XPe.png" alt="Helio Training" width="226" align="center"/> v1.0.0

---------------

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

# Exploring Promises

Basic examples and exercises for the new platform.

## Instructions

```sh
# Install dependencies
yarn
```
or
```sh
npm install
```

# Test the application
```sh
yarn test
```
or
```sh
npm test
```

## Coding Exercise

